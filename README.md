Support Message Module
=


Instalation:
-
1 . The module depends on the extension [yiisoft/yii2-httpclient](https://github.com/yiisoft/yii2-httpclient)

Either run

```
php composer.phar require --prefer-dist yiisoft/yii2-httpclient
```

or add

```
"yiisoft/yii2-httpclient": "~2.0.0"
```

to the require section of your composer.json.

2 . Insert to directory app/modules

3 . cofig.php:
```php
<?php
return [
// ...
    'modules' => [
// ...
        'sm' => [
            'class' => 'app\modules\sm\Module',
            'key' => 'Your Yandex api key',
//            'adminRule' => 'Your access rule for admin controller',
         ],
// ...
    ],
// ...
];
   
```

4 . Make migration:
```bash
$ ./yii migrate -p=@app/modules/sm/migrations
```

Using:
-
/sm - url for an authorized user.

/sm/admin - url for admin.

By default, the admin controller is available to the user with a username "admin".
