<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sm`.
 */
class m180423_131005_create_sm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sm}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'lang' => $this->string(2),
            'origin_question' => $this->text(),
            'question' => $this->text(),
            'answer' => $this->text(),
            'origin_answer' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'error' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sm');
    }
}
