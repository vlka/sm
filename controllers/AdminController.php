<?php

namespace sm\controllers;

use Yii;
use sm\models\Sm;
use sm\models\SmSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AdminController implements the CRUD actions for Sm model.
 */
class AdminController extends Controller
{
    public function init()
    {
        parent::init();

        $this->attachBehavior('AccessControl', [
            'class' => 'yii\filters\AccessControl',
            'rules' => [
                $this->module->adminRule,
            ],
        ]);
    }

    /**
     * Lists all Sm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Sm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'answer';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/sm/admin']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Sm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('sm', 'The requested page does not exist.'));
    }
}
