<?php

namespace sm\controllers;

use Yii;
use yii\web\Controller;
use sm\models\SmSearch;
use sm\models\Sm;

/**
 * Default controller for the `sm` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['user_id' => Yii::$app->user->id]);
        $dataProvider->sort = [
            'attributes' => [
                'status',
                'created_at',
                'updated_at',
            ],
        ];
        $model = new Sm([
            'user_id' => Yii::$app->user->id,
        ]);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->addFlash(Yii::t('sm', 'Message is sending'));
            return $this->refresh();
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }
}
