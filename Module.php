<?php

namespace app\modules\sm;
use Yii;

/**
 * sm module definition class
 */
class Module extends \yii\base\Module
{
    public $adminLang = 'en';
    public $api = 'sm\api\Yandex';
    public $key;
    public $adminRule;
    public $translation = [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@sm/messages',
    ];


    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'sm\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if(!$this->adminRule){
            $this->adminRule = [
                'allow' => true,
                'matchCallback' => function(){
                    return Yii::$app->user->identity->username == 'admin';
                }
            ];
        }
        Yii::setAlias('@sm', __DIR__);
        Yii::$app->i18n->translations['sm'] = $this->translation;
    }
}
