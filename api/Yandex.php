<?php

namespace sm\api;


use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Response;

class Yandex
{
    public $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';
    public $format = 'plain';
    public $key;

    public function translate($text, $lang)
    {
        $client = new Client();
        /** @var Response $response */
        $response = $client->createRequest()
            ->setUrl($this->url)
            ->setMethod('POST')
            ->setData([
                'format' => $this->format,
                'lang' => $lang,
                'text' => $text,
                'key' => $this->key,
            ])
            ->send();

        return Json::decode($response->content);
    }
}