<?php

namespace sm\models;


use app\modules\sm\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Sm
 * @package sm\models
 * @property integer $id
 * @property integer $user_id
 * @property string $lang
 * @property string $origin_question
 * @property string $question
 * @property string $answer
 * @property string $origin_answer
 * @property string $error
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Sm extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_CLOSED = 1;
    const STATUS_ERROR = 2;

    public function behaviors()
    {
        return [
            'yii\behaviors\TimestampBehavior',
        ];
    }

    public static function tableName()
    {
        return '{{%sm}}';
    }

    public function rules()
    {
        return [
            [['origin_question'], 'required'],
            [['answer'], 'required', 'on' => 'answer'],
            [['user_id'], 'integer'],
            [['origin_question', 'answer'], 'string', 'max' => 10000],
        ];
    }

    public function beforeSave($insert)
    {
        /** @var Module $module */
        $module = Yii::$app->controller->module;
        $apiClass = $module->api;
        $api = new $apiClass();
        $api->key = $module->key;
        if($insert){
            $data = $api->translate($this->origin_question, $module->adminLang);
            if($data['code'] == 200){
                $this->question = $data['text'][0];
                $this->lang = explode('-', $data['lang'])[0];
            } else {
                $this->error = $data['message'];
                $this->status = static::STATUS_ERROR;
            }
        } else {
            if($this->isAttributeChanged('answer') && $this->lang){;
                $data = $api->translate($this->answer, $this->lang);
                if($data['code'] == 200){
                    $this->origin_answer = $data['text'][0];
                } else {
                    $this->error = $data['message'];
                    $this->status = static::STATUS_ERROR;
                }
                $this->status = static::STATUS_CLOSED;
            }
        }
        return parent::beforeSave($insert);
    }

    public function getStatusList()
    {
        return [
            static::STATUS_NEW => Yii::t('sm', 'New'),
            static::STATUS_CLOSED => Yii::t('sm', 'Closed'),
            static::STATUS_ERROR => Yii::t('sm', 'Error'),
        ];
    }
}
