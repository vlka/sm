<?php
/**
 * @var $this yii\web\View
 * @var $model \sm\models\Sm
 */

use sm\models\Sm;

?>
<div class="question panel panel-primary panel-body">
    <div class="date">
        <?= Yii::$app->formatter->asDatetime($model->created_at);?>
    </div>
    <pre class="col-md-9 alert alert-<?= $model->status == Sm::STATUS_NEW ? 'danger' : 'success';?>">
        <?= $model->origin_question;?>
    </pre>
    <div class="clearfix"></div>
    <?php if($model->origin_answer):?>
        <div class="date col-md-offset-3">
            <?= Yii::$app->formatter->asDatetime($model->updated_at);?>
        </div>
        <pre class="col-md-offset-3 alert alert-info">
            <?= $model->origin_answer;?>
        </pre>
    <?php endif;?>
</div>