<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $model \sm\models\Sm
 */

use yii\widgets\ActiveForm;
use yii\widgets\ListView;

$this->title = Yii::t('sm', 'Contacting customer support');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sm-default-index">
    <h1><?= $this->title;?></h1>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '@sm/views/default/_item',
    ]);?>
    <?php if(Yii::$app->controller->module->api == 'sm\api\Yandex'):?>
        <a href="http://translate.yandex.ru/">Переведено сервисом «Яндекс.Переводчик»</a>
    <?php endif;?>
    <?php $form = ActiveForm::begin();?>
    <?= $form->field($model, 'origin_question')->textarea()->label(Yii::t('sm', 'New question'));?>
    <?= \yii\helpers\Html::submitButton(Yii::t('sm', 'Send'), ['class' => 'btn btn-md btn-success']);?>
    <?php ActiveForm::end();?>
</div>
