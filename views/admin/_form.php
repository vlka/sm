<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sm\models\Sm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$model->question):?>
        <?= $form->field($model, 'origin_question')->textarea(['rows' => 6]) ?>
    <?php else:?>
        <?= $form->field($model, 'question')->textarea(['rows' => 6]) ?>
    <?php endif;?>

    <?= $form->field($model, 'answer')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('sm', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if(Yii::$app->controller->module->api == 'sm\api\Yandex'):?>
        <a href="http://translate.yandex.ru/">Переведено сервисом «Яндекс.Переводчик»</a>
    <?php endif;?>

</div>

