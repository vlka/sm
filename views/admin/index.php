<?php

use sm\models\Sm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel sm\models\SmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('sm', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sm-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'lang',
            'origin_question:ntext',
            'question:ntext',
            'answer:ntext',
            'origin_answer:ntext',
            [
                'attribute' => 'status',
                'filter' => $searchModel->statusList,
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'style' => [
                        'width' => '90px',
                    ],
                ],
                'format' => 'raw',
                'value' => function($model){
                    if($model->status == Sm::STATUS_NEW){
                        return Html::button($model->statusList[$model->status], ['class' => 'col-md-12 btn btn-xs btn-info']);
                    }
                    if($model->status == Sm::STATUS_CLOSED){
                        return Html::button($model->statusList[$model->status], ['class' => 'col-md-12 btn btn-xs btn-success']);
                    }
                    return Html::button($model->statusList[$model->status], ['class' => 'col-md-12 btn btn-xs btn-danger']);
                }
            ],
            'error',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>
    <?php if(Yii::$app->controller->module->api == 'sm\api\Yandex'):?>
        <a href="http://translate.yandex.ru/">Переведено сервисом «Яндекс.Переводчик»</a>
    <?php endif;?>
</div>

